# Dataflow Interview Test

You have been tasked with moving some business logic from SQL to Dataflow code.

Take a look at the file `legacy.sql` and re-implement it in code.

To simplify I/O in this test, BigQuery has been replaced with local files.

* `raw.csv` - The input data
* `output.csv` - The output from the legacy system

The project is already set up to read the input, count the number of lines in the file and write that as output to a new file.

## Prerequisites

* JDK 11 ([download link](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html))

## Developer Info

Run the application either through your IDE, or via Gradle:
```bash
 ./gradlew run
```

[Protocol Buffers](https://developers.google.com/protocol-buffers/) has been included in the project set-up in case you wish to make use of it.

To generate code from the Proto files, run the Gradle task:
```bash
 ./gradlew generateProto
```

*You may need to re-sync your IDE with Gradle after the first run for the generated source directory to be recognised.*

# Implementation

You are free to decide on the implementation, making use of the provided code or not.

Write the application as you would a production app, but time-box yourself to 45 - 60 minutes.