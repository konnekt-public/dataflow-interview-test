package farfetch.interview.dataflow;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class ToStringFnTest {

    private Application.ToStringFn fn = new Application.ToStringFn();

    @Test
    void numberToString() {
        Long input = 5L;
        String expected = "5";

        String output = fn.apply(input);

        assertThat(output, is(expected));
    }

}
