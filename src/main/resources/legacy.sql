SELECT
    productidoriginal AS product_id,
    storeid as store,
    stockunitsendofperiod AS stock,
    qtysalesunitsofflinestore AS offline_total_sales,
    max(rrp) as website_price
FROM `bigquery-analytics-workbench.gold_read.factsellthru_weekly`
where datepartition = TIMESTAMP('2019-08-04')
group by productidoriginal, storeid, stockunitsendofperiod, qtysalesunitsofflinestore
order by productidoriginal
