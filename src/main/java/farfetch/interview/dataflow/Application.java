package farfetch.interview.dataflow;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        logger.info("********** STARTING UP **********");

        if (args.length == 0) {
            args = new String[]{
                    "--appName=dataflow-interview-test",
                    "--runner=DirectRunner",
            };
            logger.info("Using default args: {}", Arrays.toString(args));
        }

        var options = PipelineOptionsFactory.fromArgs(args).create();
        Pipeline pipeline = Pipeline.create(options);

        pipeline.apply(TextIO.read().from("src/main/resources/raw.csv")) // product_id,store,stock,offline_total_sales,rrp,datepartition
                .apply(Count.globally())
                .apply(MapElements.via(new ToStringFn()))
                .apply(TextIO.write().to("build/count").withSuffix(".txt").withoutSharding());

        logger.info("********** RUNNING PIPELINE **********");
        pipeline.run().waitUntilFinish();
        logger.info("**********     ALL DONE     **********");
    }

    public static class ToStringFn extends SimpleFunction<Object, String> {
        @Override
        public String apply(Object input) {
            return String.valueOf(input);
        }
    }
}
